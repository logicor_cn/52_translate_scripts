#!/usr/bin/env python

import os
import re

from yml import Yml
from smartconf import SmartConfig, is_utf8

class YmlSet:
    def __init__(self, tab=None):
        if tab is None:
            self.table = {}
        else:
            if type(tab) is not type({}):
                raise Exception("tab must be a dict")
            self.table = tab

    def loadDir(self, base_dir, type, filename_pattern, **kwargs):
        fpattern = re.compile(filename_pattern)
        for cur_dir, _, files in os.walk(base_dir):
            for f in files:
                m = fpattern.match(f)
                if m is None:
                    print("Skip file[%s] based on pattern" % f)
                    continue

                key = m.group('key')
                fpath = os.path.join(cur_dir, f)
                fname = os.path.relpath(fpath, base_dir)

                yml_object = Yml().loadFile(fpath, type, **kwargs)
                if key not in self.table:
                    self.table[key] = yml_object
                else:
                    self.table[key].merge(yml_object)

        return self

    def loadDirWithConfig(self, base_dir, **kwargs):
        sconf = SmartConfig.loadDirConf(base_dir, **kwargs)
        return self.loadDir(base_dir, **sconf)

    def saveToPath(self, dst_dir, type, **kwargs):
        if not os.path.exists(dst_dir):
            os.makedirs(dst_dir)

        for y in self.table.keys():
            f = os.path.join(dst_dir, y)
            d = os.path.dirname(f)
            if not os.path.exists(d):
                os.makedirs(d)

            basename, _ = os.path.splitext(f)

            if type == 'yml':
                fname = basename + '.yml'
                if is_utf8(kwargs['encoding']) and kwargs['fake_codec']:
                    print("Warning: fake_codec is forced off for utf-8 encoding")
                    kwargs['fake_codec'] = False
                self.table[y].saveYmlFile(fname, **kwargs)
            elif type == 'trans':
                fname = basename + '.txt'
                self.table[y].saveTransFile(fname, **kwargs)
            else:
                raise ValueError("illegal 'type'")

        sc = SmartConfig(type=type, **kwargs)
        sc.save(dst_dir)

    # Merge other into self, Yml entries will be updated with text from other
    def merge(self, other, ignore_extra=True):
        for key in other.table.keys():
            if (not ignore_extra) and (key not in self.table.keys()):
                self.table[key] = other.table[key]
                continue

            self.table[key].update(other.table[key])

        return self

    # Merge self and other into merged
    # leaving diff entries (entries not defined in other) in self
    def merge_diff(self, merged, other):
        for key in other.table.keys():
            if key not in merged.table:
                merged.table[key] = Yml()
                merged.table[key].genMapIfNone()
            self.table[key].merge_diff(merged.table[key], other.table[key])

        return self, merged

    def sanity_check(self, other, output_dir):
        for key in self.table.keys():
            if key not in other.table.keys():
                with open(os.path.join(output_dir, key + ".translation_file_missing"), "w"):
                    continue

            missing, mismatches = self.table[key].sanity_check(other.table[key])
            
            if len(missing) != 0 or len(mismatches) != 0:
                with open(os.path.join(output_dir, key + ".log"), 'w') as fp:
                    fp.write('\xEF\xBB\xBF')
                    
                    for entry in missing:
                        fp.write((u"Missing entry [%d][%s]\n" % (entry.idx, entry.text)).encode("utf-8"))

                    for orig, trans, miskeys in mismatches:
                        fp.write((u"[%d][%s] Mismatch entry: [" % (orig.idx, orig.name) + u", ".join(miskeys) + "]\n").encode("utf-8"))
                        fp.write((u"  %s\n" % orig.text).encode("utf-8"))
                        fp.write((u"  %s\n" % trans.text).encode("utf-8"))
                        fp.write("\n");

                    fp.write("\n");

        return self
