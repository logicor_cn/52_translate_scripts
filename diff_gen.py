#!/usr/bin/env python

import sys
import os
import re

from yml import Yml
from smartconf import SmartConfig
from dir_recur import Operation, dir_recur

def yml_diff(refyml, newyml):
    refTab = refyml.table
    newTab = newyml.table
    entryTab = []

    i = 0
    j = 0
    while i < len(newTab):
        jj = j
        flipover = False
        found = False
        while (not flipover and j < len(refTab)) or (flipover and j < jj):
            if newTab[i].name == refTab[j].name:
                found = True
                #if newTab[i].text.replace('"', "'").lower() != refTab[j].text.replace('"', "'").lower():
                if newTab[i].text != refTab[j].text:
                    entryTab.append(newTab[i])
                break
            j += 1
            if j == len(refTab):
                j = 0
                flipover = True
        if not found:
            entryTab.append(newTab[i])
        i += 1

    return entryTab

class OpYmlDiff(Operation):
    def __init__(self):
        self.table = {}

    def __call__(self, new_file, old_file, rel_file_path):
        if not new_file.endswith('.yml'):
            return

        if rel_file_path in self.table.keys():
            raise Exception("rel_file_path colision!")

        if not os.path.exists(old_file):
            self.table[rel_file_path] = Yml().loadYmlFile(new_file, 'cp1252', fake_codec=True).table
            return

        new_yml = Yml().loadYmlFile(new_file, 'cp1252', fake_codec=True)
        old_yml = Yml().loadYmlFile(old_file, 'cp1252', fake_codec=True)

        table = yml_diff(old_yml, new_yml)
        if len(table) > 0:
            self.table[rel_file_path] = table

    def exportdir(self, to_dir):
        if not os.path.exists(to_dir):
            os.makedirs(to_dir)

        for key, tab in self.table.items():
            fname, _ = os.path.splitext(key)
            fname = os.path.join(to_dir, fname+".txt")
            Yml(tab).saveTransFile(fname)

def main(argv):
    if not os.path.isdir(argv[1]):
        print "Error: new_dir has to be a existing folder"
    
    old_dir = argv[1]

    if not os.path.isdir(argv[2]):
        print "Error: cur_dir has to be a existing folder"
    
    new_dir = argv[2]

    to_dir = argv[3]

    op = OpYmlDiff()
    dir_recur(new_dir, old_dir, op, [])
    op.exportdir(to_dir)
    SmartConfig("trans", encoding="utf-8").save(to_dir)

if __name__ == "__main__":
    if len(sys.argv) < 4:
        print "Usage: %s old_dir new_dir to_dir" % __file__
        exit(0)

    main(sys.argv)
