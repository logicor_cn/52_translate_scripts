#!/usr/bin/env python

import sys
import os

def print_usage_exit(code):
    print "Usage: %s file offset [size]" % __file__
    exit(code)

def main(argv):
    fname = sys.argv[1]
    if not os.path.exists(fname):
        print_usage_exit(-1)
        
    fsize = os.stat(fname).st_size
    
    offset = int(argv[2], 16)
    if offset < 1024 or offset > fsize:
        print("illegal offset")
        exit(-1)
    
    if len(argv) > 3:
        size = int(argv[3])
    else:
        size = 20
    
    fp = open(fname, 'rb')
    fp.seek(offset, 0)
    str = fp.read(size)
    fp.close()
    
    for c in str:
        sys.stdout.write('%x' % ord(c))
    sys.stdout.write('\n')

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print_usage_exit(-1)

    main(sys.argv)
