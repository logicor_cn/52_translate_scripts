#!/usr/bin/env python

import sys
import os
import re

from yml import Yml
from dir_recur import Operation, dir_recur

from yml import decoding_table_char , decoding_table_regex 
regrex_rule_table=decoding_table_char+decoding_table_regex

#isalpha for both unicode and str
def isAlpha(word):
    if isinstance(word, str):
        return word.isalpha()
    elif isinstance(word, unicode):
        try:
            return word.encode('ascii').isalpha()
        except UnicodeEncodeError:
            return False

def yml_validate(refyml, newyml):
    refTab = refyml.table
    newTab = newyml.table
    miss_tab = []
    not_tran_tab = []
    char_mis_tab = []
    
    
    #looking for missing entry
    ref_Keys=[]
    cur_Keys=[]
    for ymlentry in refTab:
        ref_Keys.append(ymlentry.name)
    for ymlentry in newTab:
        cur_Keys.append(ymlentry.name)
    i = 0
    while i < len(ref_Keys):
        if ref_Keys[i] not in cur_Keys:
            miss_tab.append(refTab[i])
        i += 1
    
    
    #looking for not translated entry (detect if there is any letter)
    i = 0
    while i < len(newTab):
        if any(isAlpha(c) for c in newTab[i].text) or not newTab[i].text:
            not_tran_tab.append(newTab[i])
        i += 1
        
    #looking for special chars regrex
    i = 0
    while i < len(newTab):
        current_key=cur_Keys[i]
        if current_key in ref_Keys:
            corresponding_ref_ind=ref_Keys.index(current_key)
            new_text= newTab[i].text
            ref_text= refTab[corresponding_ref_ind].text
            for t in regrex_rule_table:
                if len(re.findall(t[0], ref_text))!=len(re.findall(t[0], new_text)):
                    char_mis_tab.append(newTab[i])
                    break
        i += 1

    return miss_tab, not_tran_tab, char_mis_tab

class OpYmlValidate(Operation):
    def __init__(self, ref_dir, cur_dir):
        self.ref_dir = ref_dir
        self.cur_dir = cur_dir
        self.table_miss = {}
        self.table_not_translated = {}
        self.table_char_mismatched = {}

    def __call__(self, ref_file, cur_file, rel_file_path):
        if rel_file_path in self.table_miss.keys() or rel_file_path in self.table_not_translated.keys() or rel_file_path in self.table_char_mismatched.keys():
            raise Exception("rel_file_path colision!")

        cur_yml = Yml().loadYmlFile(cur_file, 'cp1252', False)
        
        if os.path.exists(ref_file):
            ref_yml = Yml().loadYmlFile(ref_file, 'cp1252', False)

            miss_tab, not_tran_tab, char_mis_tab = yml_validate(ref_yml, cur_yml)
            if len(miss_tab) > 0:
                self.table_miss[rel_file_path] = miss_tab
            if len(not_tran_tab) > 0:
                self.table_not_translated[rel_file_path] = not_tran_tab
            if len(char_mis_tab) > 0:
                self.table_char_mismatched[rel_file_path] = char_mis_tab
                
        else:
            print(ref_file+' does not exist')


    def exportdir(self, to_dir):
        if not os.path.exists(to_dir):
            os.makedirs(to_dir)

        missing_dir = os.path.join(to_dir, "missing")
        if not os.path.exists(missing_dir):
            os.makedirs(missing_dir)
        for key, tab in self.table_miss.items():
            fname, _ = os.path.splitext(key)
            fname = os.path.join(missing_dir, fname+".txt")
            Yml(tab).saveTransFile(fname)

        not_translated_dir = os.path.join(to_dir, "not_translated")
        if not os.path.exists(not_translated_dir):
            os.makedirs(not_translated_dir)
        for key, tab in self.table_not_translated.items():
            fname, _ = os.path.splitext(key)
            fname = os.path.join(not_translated_dir, fname+".txt")
            Yml(tab).saveTransFile(fname)
            
        char_mis_dir = os.path.join(to_dir, "not_translated")
        if not os.path.exists(char_mis_dir):
            os.makedirs(char_mis_dir)
        for key, tab in self.table_char_mismatched.items():
            fname, _ = os.path.splitext(key)
            fname = os.path.join(char_mis_dir, fname+".txt")
            Yml(tab).saveTransFile(fname)


def main(argv):
    if not os.path.isdir(argv[1]):
        print "Error: original dir has to be a existing folder"
    
    original_dir = argv[1]

    if not os.path.isdir(argv[2]):
        print "Error: translated dir has to be a existing folder"
    
    translated_dir = argv[2]

#    if not os.path.isdir(argv[3]):
#        os.makedirs(argv[3])

    to_dir = argv[3]

    if not os.path.isdir(to_dir):
        os.makedirs(to_dir)
    op = OpYmlValidate(original_dir, translated_dir)
    dir_recur(translated_dir, original_dir, op, [])
    op.exportdir(to_dir)

if __name__ == "__main__":
    if len(sys.argv) < 4:
        print "Usage: %s ref_dir cur_dir to_dir" % __file__
        exit(0)

    main(sys.argv)