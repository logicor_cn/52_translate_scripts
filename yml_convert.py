#!/usr/bin/env python

import sys
import os
import argparse
import json

from ymlset import YmlSet

yml_encoding_map = {
    'utf8': 'utf-8',
    'u': 'utf-8',
    'c': 'cp1252',
    'g': 'gbk',
}

class unifyEncodingName(argparse.Action):
    def __init__(self, option_strings, dest, nargs=None, **kwargs):
        if nargs is not None:
            raise ValueError("nargs not allowed")

        super(unifyEncodingName, self).__init__(option_strings, dest, **kwargs)

    def __call__(self, parser, namespace, values, option_string):
        if values in yml_encoding_map.keys():
            values = yml_encoding_map[values]

        if values not in yml_encoding_map.values():
            raise ValueError('illegal encoding %s' % values)

        setattr(namespace, self.dest, values)

def main():
    parser = argparse.ArgumentParser(description='Convert between Yml/Translation/Po/Pot files')
    # arg[1]
    # output_choices = [ 'yml', 'trans' ]
    # parser.add_argument('type', type=str, nargs=1, choices=output_choices, help='Target type')
    type_parsers = parser.add_subparsers(title='type', dest='type')
    yml_parser = type_parsers.add_parser('yml', help='Convert to Yml')
    yml_encoding_choices = yml_encoding_map.keys().extend(yml_encoding_map.values())
    yml_parser.add_argument('-e', '--encoding', choices=yml_encoding_choices, \
                            action=unifyEncodingName, required=True, \
                            help='Select Yml encoding. g-GBK, u-UTF8, c-cp1252')
    yml_parser.add_argument('-f', '--fake-codec', action='store_true', \
                            help='Enable Paradox UTF-8 Fake Codec. Default off')
    trans_parser = type_parsers.add_parser('trans', help='Convert to Translation file')
    # arg[2]
    parser.add_argument('src_dir', type=str, nargs=1, help='Source directory')
    # arg[3]
    parser.add_argument('dst_dir', type=str, nargs=1, help='Destination directory')
    args = vars(parser.parse_args())

    type = args.pop('type')
    src_dir = args.pop('src_dir')[0]
    dst_dir = args.pop('dst_dir')[0]

    if not os.path.isdir(src_dir):
        print "Error: from_dir has to be a existing folder"
        exit(-1)

    if not os.path.exists(dst_dir):
        os.makedirs(dst_dir)

    YmlSet().loadDirWithConfig(src_dir).saveToPath(dst_dir, type, **args)

if __name__ == "__main__":
    main()
