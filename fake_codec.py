#!/usr/bin/env python

###############################################################################
# Deprecated
###############################################################################

def utf8_fake_encode(text):
    i = 0
    utext = u''
    while i < len(text):
        if ord(text[i]) < 128:
            utext += text[i]
            i += 1
        elif text[i] in '\xa3\xa4\xa7' and (i+1 == len(text) or ord(text[i+1]) >= 128):
            utext += text[i].decode('latin-1')
            i += 1
        else:
            try:
                utext += text[i:i+2].decode('cp1252')
            except UnicodeDecodeError:
                utext += text[i:i+2].decode('latin-1')
            i += 2

    return encode_escape(utext.encode('utf-8'))

def utf8_fake_decode(text):
    utext = decode_escape(text).decode('utf-8')

    i = 0
    ret = ''
    while i < len(utext):
        if ord(utext[i]) < 128:
            ret += str(utext[i])
            i += 1
        elif ord(utext[i]) > 255:
            try:
                ret += utext[i].encode('cp1252')
            except UnicodeEncodeError as e:
                ret += utext[i].encode('latin-1')
            i += 1
        else:
            try:
                ret += (utext[i:i+2].encode('cp1252'))
            except UnicodeEncodeError as e:
                ret += utext[i:i+2].encode('latin-1')
            i += 2
    
    return ret

###############################################################################

import sys
import os

from yml import Yml
from ms_fake_codec import ms_utf8_fake_encode, ms_utf8_fake_decode

def main(argv):
    if argv[1] == 'encode':
        text_filter = ms_utf8_fake_encode
        full_header = True
    elif argv[1] == 'decode':
        text_filter = ms_utf8_fake_decode
        full_header = False
    else:
        print "Usage: %s encode/decode from_dir to_dir" % __file__
        exit(-1)

    if not os.path.isdir(argv[2]):
        print "Error: from_dir has to be a existing folder"
        exit(-1)
    
    from_dir = argv[2]

    to_dir = argv[3]

    if not os.path.exists(to_dir):
        os.makedirs(to_dir)
    
    # TODO: finish
    
if __name__ == "__main__":
    main()
