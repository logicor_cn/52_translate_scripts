#!/usr/bin/env python

import sys
import os
import re


def main(argv):
    if not os.path.isfile(argv[1]):
        print "Error: ref_dir has to be a existing regular file"

    fname = argv[1]
    cut_size = int(argv[2]) * 1024

    fp = open(fname, 'r')
    lines = fp.readlines()
    fp.close()

    size = 0
    i = 0
    if lines[0].startswith('\xef\xbb\xbf'):
        lines[0] = lines[0][3:]
    while i < len(lines):
        m = re.match("\[\d+\];[^;]+;", lines[i])
        if m is None:
            raise Exception("Entry title matching failure")
        if re.match("^\s+$", lines[i+1]):
            print("WARN: blank entry: %s" % m.group(0))

        # update size
        size += len(lines[i+1])
        if size > cut_size:
            break

        i += 2
        while lines[i] == '\n':
            i += 1

    f_base, f_ext = os.path.splitext(fname)
    m = re.match("(.+)\.(\d+)$", f_base)
    if m is not None:
        f_idx = m.group(2)
        f_base = m.group(1)
    else:
        f_idx = '0'

    # first half
    fname = f_base + '.' + f_idx + f_ext
    fp = open(fname, 'w')
    fp.writelines(lines[:i])
    fp.close()
    
    f_idx = str(int(f_idx) + 1)
    fname = f_base + '.' + f_idx + f_ext
    fp = open(fname, 'w')
    fp.writelines(lines[i:])
    fp.close()

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print "Usage: %s file size\n\tsize - Size of first half in KB\n" % __file__
        exit(0)

    main(sys.argv)
