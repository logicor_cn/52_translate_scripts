﻿#!/usr/bin/env python

# -*- coding: utf-8 -*-

import os
import re

import polib
from smartconf import is_utf8

decoding_table_char = [
    [ u'\xa7([a-zA-Z!])', u'<A7-\\1>' ],
    [ u'\xa3([a-zA-Z]\w+)\xa3', u'<A3-\\1>' ],
    [ u'\xa4', u'<A4>' ],
]

decoding_table_regex = [
    [ u'\$([\w\d\-+/%|=\xa7]+)\$', u'<VAR-\\1>' ],
#    [ '\xc2\xa7([A-Z!])', '<A7-\\1>' ],
#    [ '\xc2\xa3(\w+)\xc2\xa3', '<A3-\\1>' ],
#    [ '\\\'', '\'' ],
#    [ '\\\\"', '"' ],
]

encoding_table = [
]

#fuck paradox
spec_char_update = [
    [ '\xa3([a-zA-Z]\w+)[^\xa3]', '\xa3\\1\xa3' ],
]

spec_chars_map = {}

latin_ascii_map = {
    '\xA0':' ',
    '\xAB':'~',

    '\xC0':'A',
    '\xC1':'A',
    '\xC2':'A',
    '\xC3':'A',
    '\xC4':'A',
    '\xC5':'A',
    '\xC6':'A',
    '\xC7':'C',
    '\xC8':'E',
    '\xC9':'E',
    '\xCA':'E',
    '\xCB':'E',
    '\xCC':'I',
    '\xCD':'I',
    '\xCE':'I',
    '\xCF':'I',

    '\xD0':'D',
    '\xD1':'N',
    '\xD2':'O',
    '\xD3':'O',
    '\xD4':'O',
    '\xD5':'O',
    '\xD6':'O',

    '\xD8':'O',
    '\xD9':'U',
    '\xDA':'U',
    '\xDB':'U',
    '\xDC':'U',
    '\xDD':'Y',
    '\xDE':'P',
    '\xDF':'B',

    '\xE0':'a',
    '\xE1':'a',
    '\xE2':'a',
    '\xE3':'a',
    '\xE4':'a',
    '\xE5':'a',
    '\xE6':'a',
    '\xE7':'c',
    '\xE8':'e',
    '\xE9':'e',
    '\xEA':'e',
    '\xEB':'e',
    '\xEC':'i',
    '\xED':'i',
    '\xEE':'i',
    '\xEF':'i',

    '\xF0':'o',
    '\xF1':'n',
    '\xF2':'o',
    '\xF3':'o',
    '\xF4':'o',
    '\xF5':'o',
    '\xF6':'o',

    '\xF8':'o',
    '\xF9':'u',
    '\xFA':'u',
    '\xFB':'u',
    '\xFC':'u',
    '\xFD':'y',
    '\xFE':'p',
    '\xFF':'y',
    
}

# compile encoding_table from decoding_table
for l in decoding_table_char + decoding_table_regex:
    lhs = l[0]
    rhs = l[1]
    
    cappat = "\(.*?\)"
    caps = re.findall(cappat, lhs)
    
    if len(caps) > 0:
        rhs = re.sub("\\\\\d", "%s", rhs) % tuple(caps)
        rhs = rhs.replace('{', '\\{')
        rhs = rhs.replace('}', '\\}')
        for i in range(1,len(caps)+1):
            lhs = re.sub(cappat, "\\\\%d" % i, lhs, 1)
            lhs = lhs.replace('\\$', '$')
    
    encoding_table.append([rhs, lhs])

for item in decoding_table_char:
    spec_chars_map[item[0][0]] = item

###############################################################################

def spec_decode_old(text, is_wchar=True):
    for t in decoding_table_regex:
        text = re.sub(t[0], t[1], text)

    i = 0
    while i < len(text):
        c = text[i]
        if ord(c) < 128:
            i += 1
            continue
        elif c in spec_chars_map.keys():
                # using chr(ord(c)) to convert unicode type to char type
                # Condition here is not sufficient, but no good way to be sure with only local information yet.
                pattern = spec_chars_map[c][0]
                repl = spec_chars_map[c][1]
                init_len = len(text)
                m = re.match(pattern, text[i:])
                if m is not None:
                    text = text[:i] + re.sub(pattern, repl, text[i:], 1)
                    i += (len(text) - init_len) + (m.end(0) - m.start(0))
                    continue
                #text = text[:i] + text[i:].replace(c, spec_chars_map[c], 1)
                #i += 4

        # fall-through default
        if is_wchar:
            text[i+1]   # there should be at least one more byte, otherwise assert for unaligned index
            i += 2
        else:           # could just be latin-1 orignal text
            i += 1

    return text

# Operates on unicode, string decoded from input encoding.
# This avoids issue with wide char on GBK input
def spec_decode_unicode(text):
    if type(text) != unicode:
        raise TypeError('Expecting unicode')
    for t in decoding_table_regex:
        text = re.sub(t[0], t[1], text)

    for t in decoding_table_char:
        text = re.sub(t[0], t[1], text)

    return text

# Operates on unicode, string decoded from input encoding.
# This avoids issue with wide char on GBK input
def spec_encode_unicode(text):
    if type(text) != unicode:
        raise TypeError('Expecting unicode')
    for t in encoding_table:
        text = re.sub(t[0], t[1], text)
    return text

# Operates on target encoding, string is converted to target encoding
# from unicode before calling this because it become impossible to
# encode to GBK if a3/a4/a7 restored into unicode string
def spec_encode(text):
    if type(text) != str:
        raise TypeError('Expecting str')
    for t in encoding_table:
        text = re.sub(t[0].encode('cp1252'), t[1].encode('cp1252'), text)
    return text

def kill_latin(text):
    for key in latin_ascii_map.keys():
        text = text.replace(key, latin_ascii_map[key])
    return text

###############################################################################

from ms_fake_codec import ms_utf8_fake_encode, ms_utf8_fake_decode
from smartconf import SmartConfig

class YmlEntry:
    def __init__(self, idx, name, version, text):
        if type(version) is not int:
            raise

        self.idx = idx
        self.name = name
        self.version = version
        self.text = text


UTF8_HEADER = '\xEF\xBB\xBF'

class Yml:
    HEADER = 'l_english:\n'
    FULL_HEADER = UTF8_HEADER + HEADER
    ENTRY_PATTERN = "\s+([^:]+?):(\d+){0,1}\s+\"(.*)\"\s*"
#    ENTRY_PATTERN = "\s+([^:]+?):(\d+){0,1}(?:\s+\"(.*)\"\s*){0,1}"

    def __init__(self, tab=None):
        if tab is None:
            self.table = []
        else:
            if type(tab) is not type([]):
                raise Exception("tab must be a list")
            self.table = tab

        self.encoding = None
        self.mapping = None

    @staticmethod
    def hasSpecialChar(text):
        return text.find('$') >= 0 or re.search('\xa7|\xa4|\xa3', text)

    def checkSpecialChars(self):
        wrong_entries = []
        for entry in self.table:
            if Yml.hasSpecialChar(entry.text):
                wrong_entries.append(entry)

        return wrong_entries

    def loadFile(self, fname, type, **kwargs):
        if type == 'yml':
            return self.loadYmlFile(fname, **kwargs)
        elif type == 'trans':
            return self.loadTransFile(fname, **kwargs)
        elif type == 'po' or type == 'pot':
            return self.loadPoFile(fname, **kwargs)
        else:
            raise ValueError

    def loadYmlFile(self, fname, encoding, fake_codec, check_spec_char=True):
        if encoding not in ['gbk', 'latin-1', 'cp1252'] and not is_utf8(encoding):
            raise Exception("unsupported encoding")

        print("\nLoading YML file: %s\n" % fname)
        if not check_spec_char:
            print("<<<<<< Warning: SPECIAL CHARACTER VALIDATION SKIPPED!!!!!!! >>>>>>")

        if is_utf8(encoding) and fake_codec:
            raise ValueError("fake_codec is illegal when writing utf-8 format")

        fp = open(fname, 'r')
        line = fp.readline()
        while line[0] == '#':
            line = fp.readline()

        if not line.endswith(Yml.HEADER):
            print line
            raise Exception("Header mismatch!")

        lines = fp.readlines()
        fp.close()

        idx = 0
        for l in lines:
            idx += 1
            if re.match("\s*#+", l) or re.match("\s*$", l):
                print("Skip line [%s]" % l.rstrip('\n'))
                continue

            m = re.match(Yml.ENTRY_PATTERN, l)
            if m is None:
                print l
                raise Exception("Pattern mismatch!")

            name = m.group(1).decode(encoding)
            ver = m.group(2)
            raw_text = m.group(3)

            if ver is None:
                ver = -1
            else:
                ver = int(ver)

            text_m = ms_utf8_fake_decode(raw_text) if fake_codec else raw_text
            text_m = text_m.decode(encoding)

            # By this point we have unified text_m format to unicode

            # upgrade \xa3 syntax
            text_mm = re.sub(u'\xa3(\w+)\xa3', u'\xa3\\1', text_m)
            text_mm = re.sub(u'\xa3(\w+)', u'\xa3\\1\xa3', text_mm)
            if text_mm != text_m:
                pass

            # Transform special characters like \xa3 \xa4 \xa7
            text = spec_decode_unicode(text_mm)

#            if re.match("^\s*$", text):
#                print "Warning: line %d: '%s': empty text" % (idx, l[:-1])

            if check_spec_char and Yml.hasSpecialChar(text):
                print "%s: line %d:\n %s" % (fname, idx+1, text)
                raise Exception("Line validation failure!")

            if text_mm != spec_encode_unicode(text):
                print text
                raise Exception("Special encoding verification failure!")

            #text.encode('gbk')      # test text

            self.table.append(YmlEntry(idx, name, ver, text))

        self.file = fname
        self.encoding = encoding

        return self

    def saveYmlFile(self, fname, encoding, fake_codec):
        if encoding not in [ 'gbk', 'cp1252', 'utf-8' ]:
            raise ValueError("Unsupported encoding %s" % encoding)

        if is_utf8(encoding) and fake_codec:
            raise ValueError("fake_codec is illegal when writing utf-8 format")

        fp = open(fname, 'wb')
        if encoding == 'utf-8':
            fp.write(UTF8_HEADER)
        fp.write(Yml.HEADER)

        for item in self.table:
            if is_utf8(encoding):
                text = spec_encode_unicode(item.text)
                text = text.encode(encoding)
            else:
                # With non-utf-8 encoding, convert text to target encoding as
                # spec_encode  might not work in GBK space (A3 A4 A7 issue)
                text = item.text.encode(encoding)
                text = spec_encode(text)
                if fake_codec:
                    text = ms_utf8_fake_encode(text)

            fp.write(" %s:%d \"%s\"\x0A" % (item.name.encode('utf-8'), item.version, text))

        fp.close()

        return self

    def loadTransFile(self, fname, encoding):
        print("\nLoading trans file: %s\n" % fname)

        fp = open(fname, 'r')
        lines = fp.readlines()
        fp.close()

        if is_utf8(encoding) and lines[0][:3] == '\xef\xbb\xbf':
            lines[0] = lines[0][3:]

        i = 0
        while i < len(lines):
            m = re.match("\[-{0,1}(\d+)\];([^;]+);([-\d]+;){0,1}\n", lines[i])
            if m is None:
                print("Corrupted entry header: in %s line %d" % (fname, i+1))
                #print('"%s"' % (lines[i].decode(encoding)))
                print("Fix the text now!")

                os.system('pause')

                fp = open(fname, 'r')
                lines = fp.readlines()
                fp.close()

                continue

            id = int(m.group(1))
            name = m.group(2).decode(encoding)
            ver = m.group(3)

            if ver is None:
                ver = -1
            else:
                ver = int(ver.rstrip(";"))

            i += 1

            m = re.match("(.+)$", lines[i])

            if m is not None:
                text = m.group(1)
            else:
                text = ""
                #print 'Warning: [%s]%s: empty (line %d)' % (id, name, i)
                #raise Exception("text missing")
            i += 1

            if i < len(lines) and re.match("\s*\n", lines[i]) is None:
                print("Corrupted entry content: in %s line %d" % (fname, i+1))
                #print('"%s"' % (lines[i].decode(encoding)))
                print("Fix the text now!")

                os.system('pause')

                fp = open(fname, 'r')
                lines = fp.readlines()
                fp.close()

                i -=2

                continue

            text = text.decode(encoding)

            self.table.append(YmlEntry(id, name, ver, text))

            while i < len(lines) and re.match("\s*\n", lines[i]):
                i += 1

        return self

    def saveTransFile(self, fname):
        if len(self.table) == 0:
            return

        fp = open(fname, 'w')
        fp.write(UTF8_HEADER)
        for item in self.table:
            fp.write("[%d];%s;%d;\n%s\n\n" % (item.idx, item.name.encode('utf-8'), item.version, item.text.encode('utf-8')))

        fp.close()

        return self

    def loadPoFile(self, fname, tag_pattern, text_key):
        print("\nLoading Po file: %s\n" % fname)

        po_file = polib.pofile(fname)
        tag_regex = re.compile(tag_pattern)

        for po_entry in po_file:
            tag = po_entry.msgctxt
            m = tag_regex.match(tag)
            idx = int(m.group(1))
            name = m.group(2)
            version = int(m.group(3))

            text = getattr(po_entry, text_key)
            text = text.replace('\n', '')   # WAR for unexpected \newline
            self.table.append(YmlEntry(idx, name, version, text))

        return self

    def genMapIfNone(self):
        if self.mapping is None:
            # One-liner is not always easier to read
            # self.mapping = dict(zip([ item.name for item in self.table], self.table))
            self.mapping = {}
            for entry in self.table:
                self.mapping[entry.name] = entry

    # Overwirte entris of self with text from other
    def update(self, other):
        if not isinstance(other, Yml):
            raise TypeError('Expecting Yml object')

        self.genMapIfNone()

        for item in other.table:
            if item.name not in self.mapping.keys():
                print("Entry '%s' not found in base YML" % (item.name.encode('cp1252')))
                continue

            #self.mapping[item.name].idx = -1
            self.mapping[item.name].text = item.text
            #self.mapping[item.name].version = item.version

    # Merge two yml object whose entry should not collide in name
    def merge(self, other):
        if not isinstance(other, Yml):
            raise TypeError('Expecting Yml object')

        self.genMapIfNone()

        for item in other.table:
            if item.name not in self.mapping.keys():
                self.table.append(item)
                self.mapping[item.name] = item
            else:
                raise ValueError("Conflict item [%s]: '%s'" % (item.name, item.text))

    # Move the entries also exist in other (according to name)
    # from self to merged with entry text from other.
    #
    # self will only have entries not in other after this
    def merge_diff(self, merged, other):
        raise Exception("Buggy function, do not use")

        if not isinstance(merged, Yml) or not isinstance(other, Yml):
            raise TypeError('Expecting Yml object')

        self.genMapIfNone()

        # FIXME: 这里没有考虑词条tag没变但text变了需要更新的情况
        for entry in other.table:
            if entry.name in self.mapping.keys():
                merged.update_entry(entry)
                self.remove_entry(entry)
            elif entry.name in merged.mapping.keys():
                merged.update_entry(entry)

    # Insert entry to self if not exist, otherwise update entry of same name
    def update_entry(self, entry):
        if not isinstance(entry, YmlEntry):
            raise TypeError('Can only remove YmlEntry from Yml')

        if entry.name in self.mapping.keys():
            self.mapping[entry.name].idx  = entry.idx
            self.mapping[entry.name].text = entry.text
            self.mapping[entry.name].version = entry.version
        else:
            self.table.append(entry)
            self.mapping[entry.name] = entry

    # remove
    def remove_entry(self, entry):
        if not isinstance(entry, YmlEntry):
            raise TypeError('Can only remove YmlEntry from Yml')

        self.table.remove(self.mapping[entry.name])
        self.mapping.pop(entry.name)

    # sanity check: check if other yml meets sanity constrains of self (always use self as reference)
    def sanity_check(self, other):
        if not isinstance(other, Yml):
            raise TypeError('Expecting Yml object')

        other.genMapIfNone()

        from collections import Counter as counter

        # Check [ ] first
        missing = []
        mismatchs = []
        for sentry in self.table:
            oentry = other.mapping[sentry.name]

            if oentry is None:
                missing.append(sentry)

            vars_self  = dict(counter(re.findall("\[(.*?)\]", sentry.text)))
            vars_other = dict(counter(re.findall("\[(.*?)\]", oentry.text)))

            if vars_self != vars_other:
                miskeys = []
                for key, count in vars_self.iteritems():
                    if key not in vars_other.keys() or count != vars_other[key]:
                        miskeys.append(key)

                for key in vars_other.keys():
                    if key not in vars_self.keys():
                        miskeys.append(key)

                mismatchs.append((sentry, oentry, miskeys))

        return missing, mismatchs
