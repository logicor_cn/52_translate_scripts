import argparse

class SplitIntoList(argparse.Action):
    def __init__(self, option_strings, dest, nargs=None, **kwargs):
        if nargs is not None:
            raise ValueError("nargs not allowed")

        super(SplitIntoList, self).__init__(option_strings, dest, **kwargs)

    def __call__(self, parser, namespace, values, option_string):
        setattr(namespace, self.dest, [ self.const(val) for val in values.split(',') ])
