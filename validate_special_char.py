#!/usr/bin/env python

import sys
import os
import re
import argparse

from yml import Yml
from ymlset import YmlSet

def main():
    parser = argparse.ArgumentParser(description='Convert between Yml and Translation file')
    # arg[1]
    parser.add_argument('src_dir', type=str, nargs=1, help='Source directory')
    # arg[2]
    parser.add_argument('report_file', type=str, nargs=1, help='Report file')
    args = parser.parse_args()

    src_dir = args.src_dir[0]
    report_file = args.report_file[0]

    if not os.path.isdir(src_dir):
        os.makedirs(src_dir)

    src_set = YmlSet().loadDirWithConfig(src_dir)

    lines = []
    for key, yml_object in src_set.table.iteritems():
        errors = yml_object.checkSpecialChars()
        if len(errors) > 0:
            lines.append('\n%s:\n' % key)
            for entry in errors:
                lines.append('[%d][%s]\n' % (entry.idx, entry.name))

    if len(lines) > 0:
        with open(report_file, 'w') as fp:
            fp.writelines(lines)

    sys.stdout.writelines(lines)

if __name__ == "__main__":
    main()
