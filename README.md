How to generate diffs for a new EU4 version
======

### Fetch original localisation files

```bash
get_original.py <path_to_eu4> ORIGINAL\<version_hash>
```

For example:
```bash
get_original.py "D:\SteamLibrary\SteamApps\common\Europa Universalis IV" "ORIGINAL\1.24.1.0_0625"
```

### Generate diff files

```bash
diff_gen.py ORIGINAL\<old_version_hash>\localisation ORIGINAL\<new_version_hash>\localisation DIFF\<old_version_hash>-<new_version_hash>
```

For example：

```bash
diff_gen.py ORIGINAL\1.23.0_1b5e\localisation ORIGINAL\1.24.1.0_0625\localisation DIFF\1.23.0_1b5e-1.24.1.0_0625
```

### Generate PO/POT files for ZANATA

```bash
po_for_zanata.py -s 10 strict pot <output_folder> DIFF/<old_version_hash>-<new_version_hash>
```

For example:

```bash
po_for_zanata.py -s 10 strict pot ZANATA/archive/1.23.0_1b5e-1.24.1.0_0625 DIFF/1.23.0_1b5e-1.24.1.0_0625
```

### Generate file list

```bash
po_list.py <pot_folder>
```

For example:

```bash
po_list.py ZANATA/archive/1.23.0_1b5e-1.24.1.0_0625
```

How to merge translations from ZANATA
======

```python
merge.py dst_dir src_dir trans_dir [trans_dir ...]
```
* dst_dir: Output path
* src_dir: Path to original localisation directory.  Directory must contain .smartconf file.  See .smartconf/ directory for examples.
* trans_dir: Path(s) to translated po files.  Can be more than one.  Each path must contain .smartconf file.  See .smartconf/ directory for examples.  
Path(s) in trans_dir are processed one by one.  **Translations in later path(s) will override the ones in previous path(s) if there is a conflict.**
* Backed up translations can be found in [52_zanata_translation](https://bitbucket.org/logicor_cn/52_zanata_translation)

File list
======
  
|File              | Description  |
|------------------|:------------:|
[a3_upgrade.py](a3_upgrade.py)           |[Obsoleted] Upgrade \xa3\w+ in yml files to \xa3\w+\xa3 in batch  
[auxiliary.py](auxiliary.py)             |Auxiliary function / classes
[charset_gen.py](charset_gen.py)         |Scan through Files to generate used character set  
[diff_gen.py](diff_gen.py)               |Generate differentiation of two version of English text  
[diff_gen2.py](diff_gen2.py)             |Generate changed text and new text of two version of English text in two separate folders  
[dir_recur.py](dir_recur.py)             |Utility for tranverse two directory synchronously, diff_gen.py, yml_convert and others utilizes this.  
[fake_codec.py](fake_codec.py)           |Obsoleted file  
[mark.py](mark.py)                       |Tag an exe file with unique mark.  For tracing leaked exe file.  
[merge.py](merge.py)                     |Merge translations  
[mirror_pick.py](mirror_pick.py)         |Pick *_l_english.yml from localisation along with other necessary files in      |mirror of the structure of given folder (usually previous version of Chinese Localisation)  
[ms_fake_codec.py](ms_fake_codec.py)     |Fake codec that calls into win32 API to mimic exactly paradox's stupid hack around cp1252 and utf8  
[po_list.py](po_list.py)                 |Generate PO file list csv with size info
[po_for_zanata.py](po_for_zanata.py)     |Generate PO/POT file for use of Zanata  
[polib.py](polib.py)                     |3rdparty po library  
[preprocessor.py](preprocessor.py)       |[Obsoleted] a3_upgrade plus  
[readhex.py](readhex.py)                 |Read the unique mark from exe file  
[sanity_check.py](sanity_check.py)       |Basic sanity check for output yml in reference to original yml
[smartconf.py](smartconf.py)             |Custom configure file used to specify file metainfo of a folder  
[split_transfile.py](split_transfile.py) |Split Translation file into 2 based on specified size  
[validate_gen.py](validate_gen.py)       |Generate 1, Missing entries in translated files 2, Untranslated entries in translated files 3, Entries in translated files  that has mismatch regex pattern with original file
[validate_special_char.py](validate_special_char.py)  |Validate that special characters are all taken care of.
[yml.py](yml.py)                         |Yml File class, the core everything was built around  
[yml_convert.py](yml_convert.py)         |Convert between Yml file and Translation file  
[yml_filter.py](yml_filter.py)           |Filter entries from Yml file based by matching entry name to given regex pattern  
[ymlset.py](ymlset.py)                   |Collection of Yml objects, used to represent a folder of Yml or Translation files  
[ymlvector.py](ymlvector.py)             |Vectorization of 2 - 4 YmlSet, mainly used to pair original text and translation for generating PO file.
