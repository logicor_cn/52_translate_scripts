#!/usr/bin/env python

import os
import argparse

from yml import Yml
from ymlset import YmlSet
from ymlvector import YmlVector
from auxiliary import SplitIntoList
from smartconf import SmartConfig

def main():
    parser = argparse.ArgumentParser(description='Convert Yml and Translation file to PO for use of Zanata', formatter_class=argparse.RawDescriptionHelpFormatter)
    # arg[1]
    parser.add_argument('mode', type=str, nargs=1, choices=YmlVector.MODE_CHOICES, help='Yml vectorizing mode')
    # arg[2]
    output_choices = [ 'po', 'pot', 'both' ]
    parser.add_argument('ext', type=str, nargs=1, choices=output_choices, help='output file type')
    # arg[3]
    parser.add_argument('dst_dir', type=str, nargs=1, help='Source directory')
    # arg[4]
    parser.add_argument('src_dirs', type=str, nargs='+', help='Destination directory(s)')
    # Options
    parser.add_argument('-s', '--split-size', type=int, help='Split file by source text size, in KB')
    parser.add_argument('-f', '--flags', const=str, action=SplitIntoList, help="flags, split by ','")
    args = parser.parse_args()

    mode = args.mode[0]
    o_ext = [ 'po', 'pot' ] if args.ext[0] == 'both' else args.ext
    dst_dir = args.dst_dir[0]
    src_dirs = args.src_dirs
    split_size = args.split_size
    flags = args.flags

    if len(src_dirs) > 2:
        print("Maximum of 2 columns of input only for now, got %d" % len(src_dirs))
        exit(-1)

    for srcdir in src_dirs:
        if not os.path.isdir(srcdir):
            print "Error: src_dir* has to be a existing folder"
            exit(-1)

    if not os.path.exists(dst_dir):
        os.makedirs(dst_dir)

    yml_sets = []
    for srcdir in src_dirs:
        yml_sets.append(YmlSet().loadDirWithConfig(srcdir))

    # This part probably should go to YmlVector or YmlVectorSet or something...
    for name, yml_object in yml_sets[0].table.iteritems():
        ymls = [ yml_object ]
        if len(yml_sets) > 1:
            if mode == "trim":
                if name not in yml_sets[1].table.keys():
                    continue

                for yml_set in yml_sets[1:]:
                    ymls.append(yml_set.table[name])
            elif mode == "strict":
                for yml_set in yml_sets[1:]:
                    ymls.append(yml_set.table[name])
            elif mode == "permissive":
                for yml_set in yml_sets[1:]:
                    if name in yml_set.table.keys():
                        ymls.append(yml_set.table[name])
                    else:
                        ymls.append(Yml())
            else:
                raise Exception("Supposedly unreachable code")

        # FIXME: this does not handle subdir scenario which we do not need in foreseeable future
        yml_vector = YmlVector().loadYmlObjects(ymls, mode)

        if split_size is not None:
            yml_vectors = yml_vector.split(split_size)
            po_files = [ vector.genPOFile(flags=flags) for vector in yml_vectors ]
            for i, po_file in enumerate(po_files):
                if po_file is None:
                    continue
                for ext in o_ext:
                    fname = os.path.join(dst_dir, '%s.%d.%s' % (name, i, ext))
                    po_file.save(fname)
        else:
            po_file = yml_vector.genPOFile(flags=flags)
            if po_file is None:
                continue
            for ext in o_ext:
                fname = os.path.join(dst_dir, '%s.%s' % (name, ext))
                po_file.save(fname)

    SmartConfig(o_ext[0]).save(dst_dir)

if __name__ == "__main__":
    main()
