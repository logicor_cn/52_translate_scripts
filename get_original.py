#!/usr/bin/env python

import sys
import os
import re
import shutil

from smartconf import SmartConfig

eu4_full_dirs_table = {
#    "common\countries" : None,
    "localisation" : ".*_l_english.*\.yml",
}

def full_copy(from_dir, to_dir, filter=None):
    for cur_dir, sub_dirs, sub_files in os.walk(from_dir):
        rel_cur_dir = os.path.relpath(cur_dir, from_dir)
        to_cur_dir = os.path.join(to_dir, rel_cur_dir)

        if not os.path.exists(to_cur_dir):
            print "Making Directory [%s]" % to_cur_dir
            os.mkdir(to_cur_dir)
        elif not os.path.isdir(to_cur_dir):
            print "Error: could not create [%s], non-directory file of same name exists" % to_cur_dir
            continue

        for f in sub_files:
            if filter is not None:
                if not re.match(filter, f):
                    continue
            from_file = os.path.join(cur_dir, f)
            if os.path.exists(from_file):
                to_file = os.path.join(to_cur_dir, f)
                #print "Copying [%s] to [%s]..." % (from_file, to_file)
                shutil.copyfile(from_file, to_file)
            else:
                print "File: %s not found, skipping" % (from_file)

def main(argv):
    if not os.path.isdir(argv[1]):
        print "Error: from_dir has to be a existing folder"

    from_dir = argv[1]

    if not os.path.isdir(argv[2]):
        os.makedirs(argv[2])

    to_dir = argv[2]

    full_dirs_table = eu4_full_dirs_table
    full_dirs = full_dirs_table.keys()

    for d in full_dirs:
        fromd = os.path.join(from_dir, d)
        tod = os.path.join(to_dir, d)
        full_copy(fromd, tod, full_dirs_table[d])
        conf = SmartConfig("yml", fake_codec=True, encoding="cp1252")
        conf.save(tod)

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print "Usage: %s from_dir to_dir" % __file__
        exit(0)

    main(sys.argv)
