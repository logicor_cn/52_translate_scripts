﻿#!/usr/bin/env python

import os
import re

from polib import POFile, POEntry
from yml import Yml, YmlEntry

event_pattern = re.compile("(.+\.\d+)\.\w+")
event_pattern2 = re.compile(".*EVT\w+(\d+)")
def isNameRelated(prev, current):
    # TODO: add back type check (str / unicode)
    if type(prev) != type(current):
        raise TypeError

    # TODO: get rid of duplicated matching
    # Test for event special pattern first where rule is more strict
    m = event_pattern.match(prev)
    if m is not None:
        n = event_pattern.match(current)
        if n is not None and m.group(1) == n.group(1):
            return True
        return False

    # Similarly test for event special pattern 2
    m = event_pattern2.match(prev)
    if m is not None:
        n = event_pattern.match(current)
        if n is not None and m.group(1) == n.group(1):
            return True
        return False

    if current == "desc_" + prev:
        return True

    # Test for general pattern
    len_p = len(prev)
    len_c = len(current)
    for i in range(min(len_p, len_c)):
        if prev[i] != current[i]:
            break

    if i > 0 and min(len_p, len_c) - i <= min(8, i):
        return True

    return False

class YmlEntryVector:
    def __init__(self, entries):
        for item in entries:
            if not isinstance(item, YmlEntry):
                raise TypeError("Input not a instance of YmlEntry")

        self.vector = entries

class YmlVector:
    def __init__(self):
        self.list = []

    MIN_VECTOR_WIDTH = 1
    MAX_VECTOR_WIDTH = 4
    MODE_CHOICES = [
        'strict',
        'permissive',
        'trim',
        ]

    # ymls, 暨输入的不同内容yml的顺序约定
    # [ 英文原文 ]
    # [ 英文原文, 中文译文 ]
    # [ 英文原文，中文译文， 旧英文原文 ]
    # [ 英文原文，中文译文， 旧英文原文， 旧中文译文 ]
    # mode:
    #   "strict"     : Entries in all ymls have to have matching names in same order
    #   "permissive" : Allows ymls[1:] miss entries from ymls[0]
    #   "trim"       : Trim other ymls to fit ymls[1]
    def loadYmlObjects(self, ymls, mode):
        if mode not in self.MODE_CHOICES:
            raise ValueError("Unknown mode %s" % mode)

        for item in ymls:
            if not isinstance(item, Yml):
                raise TypeError("Input not a instance of Yml")

        width = len(ymls)
        if width < self.MIN_VECTOR_WIDTH or width > self.MAX_VECTOR_WIDTH:
            raise ValueError("invalid vector width %d" % width)

        if mode != 'strict':
            for yml in ymls:
                yml.genMapIfNone()

        orig_yml = ymls[0]
        for i, orig_entry in enumerate(orig_yml.table):
            entries = [ orig_entry ]

            # TODO: clean up this mess
            if len(ymls) > 1:
                if mode == "trim":
                    # trim right here
                    if not ymls[1].mapping.has_key(orig_entry.name):
                        continue

                    for yml_obj in ymls[1:]:
                        if yml_obj.mapping.has_key(orig_entry.name):
                            cur_entry = yml_obj.mapping[orig_entry.name]
                        else:
                            cur_entry = YmlEntry(orig_entry.idx, orig_entry.name, orig_entry.version, '')
                        entries.append(cur_entry)
                else:
                    for yml_obj in ymls[1:]:
                        if mode == 'strict':
                            cur_entry = yml_obj.table[i]
                            if cur_entry.name == orig_entry.name:
                                entries.append(cur_entry)
                            else:
                                raise ValueError("All operend ymls should be identical in name of entries ordering")
                        elif mode == 'permissive':
                            if yml_obj.mapping.has_key(orig_entry.name):
                                cur_entry = yml_obj.mapping[orig_entry.name]
                            else:
                                cur_entry = YmlEntry(orig_entry.idx, orig_entry.name, orig_entry.version, '')
                            entries.append(cur_entry)
                        else:
                            raise Exception("Supposedly unreachable code")

            self.list.append(YmlEntryVector(entries))

        return self

    def genPOFile(self, **kwags):
        if len(self.list) == 0:
            return None

        po_file = POFile()
        width = len(self.list[0].vector)

        for entry in self.list:
            vector = entry.vector
            if len(vector) != width:
                raise ValueError("Vector width mismatch")

            ctorArgs = {
                'encoding' : 'utf-8',
                'msgctxt' : "[%d][%s][%d]" % (vector[0].idx, vector[0].name.decode('utf-8'), vector[0].version),
                'msgid' : vector[0].text,
            }
            if width >= 2:
                ctorArgs['msgstr'] = vector[1].text
            if width >= 3:
                ctorArgs['previous_msgid'] = vector[2].text
            if width == 4:
                ctorArgs['previous_msgstr'] = vector[3].text

            ctorArgs.update(kwags)

            po_file.append(POEntry(**ctorArgs))

        return po_file

    # size in KB
    def split(self, size):
        size = size * 1024

        new_vector = YmlVector()
        total_size = 0
        last = None
        output = []

        for entry in self.list:
            yml_entry = entry.vector[0]
            project_size = total_size + len(yml_entry.text)
            if project_size < size or (last is not None and isNameRelated(last, yml_entry.name)):
                # size not reached or entry is grouped with last entry
                new_vector.list.append(entry)
                total_size = project_size
                last = yml_entry.name
            else:
                output.append(new_vector)
                new_vector = YmlVector()
                new_vector.list.append(entry)
                total_size = 0
                last = None

        output.append(new_vector)

        return output
