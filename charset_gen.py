#!/usr/bin/env python

import sys
import os
import os.path

from yml import Yml
from ymlset import YmlSet

def print_usage():
    print("Usage: %s <output_file> src_dir0 [src_dir1 [src_dir2 [...]]]" % __file__)

def main(argv):
    if len(argv) < 3:
        print_usage()
        exit(-1)
    
    o_fname = argv[1]
    src_dirs = argv[2:]

    if os.path.exists(o_fname):
        if os.path.isfile(o_fname):
            print("File [%s] already exists, are you sure you want to overwrite?[y/N]" % o_fname)
            overwrite = sys.stdin.readline().rstrip('\r\n')
            if overwrite != 'y' and overwrite != 'Y':
                exit(0)
        else:
            print("Cannot write to existing non-file path [%s], exiting")
            exit(-1)

    for s_dir in src_dirs:
        if not os.path.isdir(s_dir):
            print "Error: src_dir* has to be a existing folder"
            exit(-1)

    charset = set()
    for s_dir in src_dirs:
        yml_set = YmlSet().loadTransFromPath(s_dir, 'utf-8')
        for yml_object in yml_set.table.values():
            for entry in yml_object.table:
                for uc in entry.text.decode('utf-8'):
                    charset.add(uc)

    fp = open(o_fname, 'w')
    fp.write(u''.join(charset).encode('utf-8'))
    print('Total %d characters have been writtern to %s' % (len(charset), o_fname))

if __name__ == "__main__":
    main(sys.argv)
