#!/usr/bin/env python

import sys
import os
import hashlib

def print_usage_exit(code):
    print "Usage: %s exe offset seed" % __file__
    exit(code)

def main(argv):
    fexe = sys.argv[1]
    if not os.path.exists(fexe):
        print_usage_exit(-1)
        
    fsize = os.stat(fexe).st_size
    
    offset = int(argv[2], 16)
    if offset < 1024 or offset > fsize:
        print("illegal offset")
        exit(-1)
    
    sha = hashlib.sha1(argv[3])
        
    fp = open(fexe, 'rb')
    execode = fp.read()
    fp.close()
    
    fp = open('eu4.hh.' + sha.hexdigest() + ".exe", 'wb')
    mark = sha.digest()
    fp.write(execode[:offset] + mark + execode[offset+len(mark):])
    fp.close()
    
    print(sha.hexdigest())

if __name__ == "__main__":
    if len(sys.argv) < 4:
        print_usage_exit(-1)

    main(sys.argv)
