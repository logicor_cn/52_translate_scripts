#!/usr/bin/env python

import sys
import os
import re
import argparse
import json

from yml import Yml
from dir_recur import dir_recur
from smartconf import SmartConfig, is_utf8

class NameFilter:
    def __init__(self, pattern, encoding, fake_codec):
        self.pattern = re.compile(pattern)
        self.encoding = encoding
        self.fake_codec = fake_codec

    def __call__(self, inf, outf, rel_path):
        _, ext = os.path.splitext(inf)
        if ext != '.yml':
            return

        if not os.path.exists(os.path.dirname(outf)):
            os.makedirs(dir_path)

        yml_object = Yml().loadYmlFile(inf, self.encoding, self.fake_codec)
        yml_object.table = [ \
            entry for entry in yml_object.table \
            if self.pattern.match(entry.name) is not None \
            ]
        yml_object.saveYmlFile(outf, self.encoding, self.fake_codec)

def main():
    parser = argparse.ArgumentParser(description='Convert between Yml and Translation file')
    # arg[1]
    parser.add_argument('src_dir', type=str, nargs=1, help='Source directory')
    # arg[2]
    parser.add_argument('dst_dir', type=str, nargs=1, help='Destination directory')
    # arg[3]
    parser.add_argument('pattern', type=str, nargs='?', default='[_0-9A-Z]+', help="Regex filter pattern, e.g. '[_0-9A-Z]+'")
    # Options
    parser.add_argument('-e', '--encoding', required=True, help='Select Yml encoding. g-GBK, u-UTF8, c-cp1252')
    parser.add_argument('-f', '--fake-codec', action='store_true', help='Enable Paradox UTF-8 Fake Codec. Default off')
    args = parser.parse_args()

    src_dir = args.src_dir[0]
    dst_dir = args.dst_dir[0]
    pattern = args.pattern
    fake_codec = args.fake_codec

    if args.encoding == 'c' or args.encoding == 'cp1252':
        encoding = 'cp1252'
    elif args.encoding == 'g' or args.encoding == 'gbk':
        encoding = 'gbk'
    elif args.encoding == 'u' or is_utf8(args.encoding):
        encoding = 'utf-8'
    else:
        print("Unsupported encoding %s selected" % args.encoding)
        parser.print_help()
        exit(-1)

    if not os.path.isdir(src_dir):
        print "Error: from_dir has to be a existing folder"
        exit(-1)

    if not os.path.exists(dst_dir):
        os.makedirs(dst_dir)

    # process recursively into direcotry
    dir_recur(src_dir, dst_dir, NameFilter(pattern, encoding, fake_codec), [])

if __name__ == "__main__":
    main()
