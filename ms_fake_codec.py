#!/usr/bin/env python

###############################################################################
#
# Invoke ms codec for fake encoding
#
###############################################################################

import ctypes
from ctypes import wintypes

codepages = {
    "cp1252": 1252,
    "utf-8": 65001,
}

def encode_escape(text):
    # escape stupid slashes
    ret = ""
    i = 0
    while i < len(text):
        if text[i] == '\\':
            if i == len(text)-1:
                ret += '\\\\'
                i += 1
            elif text[i+1] not in [ '"', '"', 'n' ]:
                ret += '\\\\'
                i += 1
            else:
                ret += text[i:i+2]
                i += 2
        elif text[i] == '"':
            ret += '\\"'
            i += 1
        else:
            ret += text[i]
            i += 1
            
    return ret

def decode_escape(text):
#   TODO: put real escape handler here!
    return text.replace("\\\\", "\\") \
               .replace("\\'", "'")   \
               .replace('\\"', '"')

def msDecode(text, encoding):
    MultiByteToWideChar = ctypes.windll.kernel32.MultiByteToWideChar
    MultiByteToWideChar.argtypes = [wintypes.UINT, wintypes.DWORD,
                                    wintypes.LPCSTR, ctypes.c_int,
                                    wintypes.LPWSTR, ctypes.c_int]
    MultiByteToWideChar.restype = ctypes.c_int

    buf_size = 4*len(text)
    buf = ctypes.create_unicode_buffer(buf_size)
    
    codepage = codepages[encoding]
    ret = MultiByteToWideChar(codepage, 0, text, len(text), buf, buf_size)
    
    if not ret > 0:
        raise Exception("WTF")
    
    return buf.value
    
def msEncode(text, encoding):
    WideCharToMultiByte = ctypes.windll.kernel32.WideCharToMultiByte
    WideCharToMultiByte.argtypes = [wintypes.UINT, wintypes.DWORD,
                                    wintypes.LPCWSTR, ctypes.c_int,
                                    wintypes.LPSTR, ctypes.c_int,
                                    wintypes.LPCSTR, ctypes.c_bool]
    WideCharToMultiByte.restype = ctypes.c_int

    buf_size = 4*len(text)
    buf = ctypes.create_string_buffer(buf_size)
    
    codepage = codepages[encoding]
    ret = WideCharToMultiByte(codepage, 0, text, len(text), buf, buf_size, None, None)
    
    if not ret > 0:
        raise Exception("WTF")

    return buf.value

def ms_utf8_fake_encode(text):
    if len(text) == 0:
        return text

    ucs2 = msDecode(text, 'cp1252')
    text = msEncode(ucs2, 'utf-8')

    return encode_escape(text)

def ms_utf8_fake_decode(text):
    if len(text) == 0:
        return text

    ucs2 = msDecode(text, 'utf-8')
    text = msEncode(ucs2, 'cp1252')

    return decode_escape(text)

###############################################################################
