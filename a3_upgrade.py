#!/usr/bin/env python

import sys
import os
import re

from yml import Yml

def a3_upgrade(fname, fout):
    out = []
    fp = open(fname, 'r')
    line = fp.readline()
    while line[0] == '#':
        out.append(line)
        line = fp.readline()

    if not line.endswith(Yml.HEADER):
        print line
        raise Exception("Header mismatch!")

    out.append(line)
    lines = fp.readlines()
    fp.close()

    for l in lines:
        l = re.sub('\xc2\xa3(\w+)\xc2\xa3', '\xc2\xa3\\1', l)
        l = re.sub('\xc2\xa3(\w+)', '\xc2\xa3\\1\xc2\xa3', l)
        out.append(l)

    fout = open(fout, 'w')
    fout.writelines(out)
    fout.close()

def main(argv):
    if not os.path.isdir(argv[1]):
        print "Error: ref_dir has to be a existing folder"

    ref_dir = argv[1]

    if not os.path.isdir(argv[2]):
        print "Error: to_dir has to be a existing folder"

    to_dir = argv[2]

    for root, dirs, files in os.walk(ref_dir):
        rel_path = os.path.relpath(root, ref_dir)
        to_path = os.path.join(to_dir, rel_path)

        if not os.path.exists(to_path):
            os.makedirs(to_path)

        for f in files:
            if f.lower().endswith(".yml"):
                to_f = os.path.join(to_path, f)
                f = os.path.join(root, f)
                print("Processing " + f)
                a3_upgrade(f, to_f)

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print "Usage: %s from_dir to_dir" % __file__
        exit(0)

    main(sys.argv)
