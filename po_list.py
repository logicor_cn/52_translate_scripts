#!/usr/bin/env python

import os
import argparse
import polib

def main():
    parser = argparse.ArgumentParser(description='Generate file list with size')
    # arg[1]
    parser.add_argument('path', type=str, nargs=1, help='PO Directory')
    args = parser.parse_args()

    path = args.path[0]
    outfile = os.path.join(path, '_list.csv')

    if not os.path.isdir(path):
        print "Error: path has to be a existing folder"
        exit(-1)

    file_table = []
    for cur_dir, sub_dirs, sub_files in os.walk(path):
        rel_cur_dir = os.path.relpath(cur_dir, path)

        for f in sub_files:
            if f.endswith('.pot'):
                f_rel = os.path.relpath(os.path.join(rel_cur_dir, f))
                f_path = os.path.join(path, f_rel)
                pf = polib.pofile(f_path)
                size = 0
                for entry in pf:
                    size += len(entry.msgid)
                file_table.append([f_rel[:-4], size])

    with open(outfile, 'w') as fp:
        for row in file_table:
            fp.write('%s,%d\n' % (row[0], row[1]))

if __name__ == "__main__":
    main()
