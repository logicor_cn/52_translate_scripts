#!/usr/bin/env python

import sys
import os

class Operation:
    def __init__(self):
        pass
    
    def __call__(self, ref_file, op_file, rel_file_path):
        raise

def dir_recur(ref_dir, op_dir, operation, exclude=[]):
    for cur_dir, sub_dirs, sub_files in os.walk(ref_dir):
        rel_cur_dir = os.path.relpath(cur_dir, ref_dir)

        if rel_cur_dir in exclude:
            del sub_dirs[:]
            continue

        op_cur_dir = os.path.join(op_dir, rel_cur_dir)

        if not os.path.isdir(op_cur_dir):
            print "Error: could not find directory[%s]" % op_cur_dir
            continue

        for f in sub_files:
            f_rel = os.path.relpath(os.path.join(rel_cur_dir, f))
            ref_file = os.path.join(ref_dir, f_rel)
            op_file = os.path.join(op_dir, f_rel)
            operation(ref_file, op_file, f_rel)
