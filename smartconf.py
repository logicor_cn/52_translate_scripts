#!/usr/bin/env python

import os
import json

SUPPORTED_TYPES = ['yml', 'trans', 'po', 'pot']
DEFAULT_FILENAME = '.smartconf.json'
DEFAULT_CONFIGS = {
    'yml': {
        "filename_pattern": "(?P<key>.+?)(?:\\.(\\d+)){0,1}.yml$",
    },
    'trans': {
        "filename_pattern": "(?P<key>.+?)(?:\\.(\\d+)){0,1}.txt$",
        "encoding":         'utf-8',
    },
    'po': {
        "filename_pattern": "(?P<key>.+?)(?:\\.(\\d+)){0,1}.po$",
        "tag_pattern":      "\\[(\\d+)\\]\\[(.+?)\\]\\[([-\\d]+)\\]",
        "text_key":         'msgstr',
    },
    'pot': {
        "filename_pattern": "(?P<key>.+?)(?:\\.(\\d+)){0,1}.pot$",
        "tag_pattern":      "\\[(\\d+)\\]\\[(.+?)\\]\\[([-\\d]+)\\]",
        "text_key":         "msgid",
    },
}

def is_utf8(encoding):
    return encoding.lower() in ['utf-8', 'utf_8', 'utf8', 'u8']

class SmartConfig(dict):
    def __init__(self, type, **kwargs):
        if type not in SUPPORTED_TYPES:
            raise ValueError("unknown type")

        # We need type immediately
        self['type'] = type

        # Fill in default values based on type
        self.update(DEFAULT_CONFIGS[type])

        for key, value in kwargs.iteritems():
            self[key] = value

    def save(self, base_dir, config_fname = DEFAULT_FILENAME):
        fname = os.path.join(base_dir, config_fname)
        with open(fname, 'w') as f:
            json.dump(self, f)

    def valid(self):
        if self['type'] not in SUPPORTED_TYPES:
            return False

        if self['type'] == 'trans':
            if not is_utf8(self['encoding']):
                return False

        return True

    @staticmethod
    def loadDirConf(base_dir, config_fname = DEFAULT_FILENAME):
        fname = os.path.join(base_dir, config_fname)
        if not os.path.isfile(fname):
            raise Exception("Cannot find smartconfig file %s" % fname)

        return SmartConfig.load(fname)

    @staticmethod
    def load(fname):
        with open(fname, 'r') as f:
            data = json.load(f)
        sc = SmartConfig(**data)
        if not sc.valid():
            raise ValueError('invalid config data %s' % str(data))

        return sc