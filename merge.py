#!/usr/bin/env python

import sys
import os
import re
import argparse

from yml import Yml
from ymlset import YmlSet

def main():
    parser = argparse.ArgumentParser(description='Merge translation files')
    # arg[1]
    parser.add_argument('dst_dir', type=str, nargs=1, help='Destination directory')
    # arg[2]
    parser.add_argument('src_dir', type=str, nargs=1, help='Source directory')
    # arg[3]
    parser.add_argument('trans_dir', type=str, nargs='+', help='Translation directory(s)')
    parser.add_argument('-d', '--diff-dir', type=str, help='Specifiy a path to store differentiation where original text is not covered by translation files.  Merge does not contain diff text in this mode.')
    args = parser.parse_args()
    
    to_dir = args.dst_dir[0]
    ref_dir = args.src_dir[0]
    trans_dirs = args.trans_dir
    diff_dir = args.diff_dir

    if not os.path.isdir(to_dir):
        os.makedirs(to_dir)

    if not os.path.isdir(ref_dir):
        print "Error: ref_dir has to be an existing folder"
        exit(-1)

    for d in trans_dirs:
        if not os.path.isdir(d):
            print "Error: trans_dir has to be an existing folder"

    refset = YmlSet().loadDirWithConfig(ref_dir)

    if diff_dir is None:
        for d in trans_dirs:
            transet = YmlSet().loadDirWithConfig(d)
            refset.merge(transet)

        refset.saveToPath(to_dir, type='yml', encoding='utf-8', fake_codec=False)
    else:
        mergedset = YmlSet()
        for d in trans_dirs:
            transet = YmlSet().loadDirWithConfig(d)
            refset.merge_diff(mergedset, transet)

        refset.saveToPath(diff_dir, type='yml', encoding='utf-8', fake_codec=False)
        mergedset.saveToPath(to_dir, type='yml', encoding='utf-8', fake_codec=False)

if __name__ == "__main__":
    main()
