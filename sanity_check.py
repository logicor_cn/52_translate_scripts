#!/usr/bin/env python

import sys
import os
import re
import argparse

from yml import Yml
from ymlset import YmlSet

def main():
    parser = argparse.ArgumentParser(description='Merge translation files')
    # arg[1]
    parser.add_argument('src_dir', type=str, nargs=1, help='Source directory')
    # arg[2]
    parser.add_argument('trans_dir', type=str, nargs=1, help='Translation directory(s)')
    # arg[3]
    parser.add_argument('output_dir', type=str, nargs=1, help='Output File')
    args = parser.parse_args()
    
    ref_dir = args.src_dir[0]
    trans_dir = args.trans_dir[0]
    output_dir = args.output_dir[0]

    if not os.path.isdir(ref_dir):
        print "Error: ref_dir has to be an existing folder"
        exit(-1)

    if not os.path.isdir(trans_dir):
        print "Error: trans_dir has to be an existing folder"

    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)

    if len(os.listdir(output_dir)) != 0:
        answer = None
        while answer is None:
            sys.stdout.write("Output directory [%s] not empty, continue? [y/n]" % output_dir)
            answer = sys.stdin.readline().rstrip('\n')
            if answer.lower() == 'n' or answer.lower() == 'no':
                exit(0)
            elif answer.lower() != 'y' and answer.lower() != 'yes':
                answer = None

    refset = YmlSet().loadDirWithConfig(ref_dir)
    transet = YmlSet().loadDirWithConfig(trans_dir)
    refset.sanity_check(transet, output_dir)

if __name__ == "__main__":
    main()
