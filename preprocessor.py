#!/usr/bin/env python

import sys
import os
import re

from yml import Yml

special_char_table = [
	['\xc2\xad' , '-']
]

def a3_upgrade(line):
	l = re.sub('\xc2\xa3(\w+)\xc2\xa3', '\xc2\xa3\\1', line)
	l = re.sub('\xc2\xa3(\w+)', '\xc2\xa3\\1\xc2\xa3', l)
	return l

def special_char_convert(line):
	l = line
	for tup in special_char_table:
		l = l.replace(tup[0], tup[1])
	return l

def preprocess(fname, fout):
    out = []
    fp = open(fname, 'r')
    line = fp.readline()
    while line[0] == '#':
        out.append(line)
        line = fp.readline()
    
    if not line.endswith(Yml.HEADER):
        print line
        raise Exception("Header mismatch!")
    
    out.append(line)
    lines = fp.readlines()
    fp.close()
    
    for l in lines:
        l = a3_upgrade(l)
        l = special_char_convert(l)
        out.append(l)

    fout = open(fout, 'w')
    fout.writelines(out)
    fout.close()

def main(argv):
    if not os.path.isdir(argv[1]):
        print "Error: ref_dir has to be a existing folder"
    
    ref_dir = argv[1]

    if not os.path.isdir(argv[2]):
        print "Error: to_dir has to be a existing folder"

    to_dir = argv[2]
    
    for root, dirs, files in os.walk(ref_dir):
        rel_path = os.path.relpath(root, ref_dir)
        to_path = os.path.join(to_dir, rel_path)
        
        if not os.path.exists(to_path):
            os.makedirs(to_path)
        
        for f in files:
            to_f = os.path.join(to_path, f)
            f = os.path.join(root, f)
            print("Processing " + f)
            preprocess(f, to_f)

if __name__ == "__main__":
    if len(sys.argv) < 3:
        print "Usage: %s ref_dir to_dir" % __file__
        exit(0)

    main(sys.argv)
